import os
import time
from click import command
from django.contrib.auth.models import User
from django.http import QueryDict
from requests import delete
from rest_framework.response import Response
from rest_framework.views import APIView
from accounts.models import Member, Project
from rest_framework import permissions

from rest_framework.authentication import SessionAuthentication, BasicAuthentication  
class CsrfExemptSessionAuthentication(SessionAuthentication): 
 
    def enforce_csrf(self, request): 
        return  # To not perform the csrf check previously happening

class ProjectInfo(APIView):
    def get(self, request, pk):
        members = Member.objects.filter(pid=pk)
        res = []
        for membership in members:
            res.append({"id": membership.uid.id, "username": membership.uid.username, "privilege": membership.privilege})
        return Response({
            "message":"success",
            "pid": pk,
            "data":res
        })

class CreateProject(APIView):
    authentication_classes = (CsrfExemptSessionAuthentication, BasicAuthentication)
    def post(self,request,format=None):
        user = request.user
        if not user.is_authenticated:
            return Response({
                "message":"failure",
                "data":{
                    "detail":"用户未登录"
                }
            })
        else:
            data = request.data
            pname = data.get("pname")
            if Project.objects.filter(pname=pname).exists():
                return Response({
                "message":"failure",
                "data":{
                    "detail":"项目名已存在"
                }
                })
            else:
                pid=Project.objects.create(pname=pname).id
                command="docker run -d -p " + str(20000+pid)+":22 --name="+pname+" stdc /usr/sbin/sshd -D"
                os.system(command)
                command='docker cp /home/mjy/coconow/accounts/walkTree.py '+pname+':/'
                os.system(command)

                Member.objects.create(pid=Project.objects.get(pname=pname), uid=request.user, privilege="owner")
                return Response({
                "message":"success",
                "data":{
                    "pid":pid
                }
                })

class RemoveProject(APIView):
    authentication_classes = (CsrfExemptSessionAuthentication, BasicAuthentication)
    def delete(self,request,format=None):
        user = request.user
        if not user.is_authenticated:
            return Response({
                "message":"failure",
                "data":{
                    "detail":"用户未登录"
                }
            })
        
        pid = request.GET["pid"]
        if Project.objects.filter(pk=pid).count()==0:
            return Response({
                "message":"failure",
                "data":{
                    "detail":"项目不存在"
                }
            })

        member=Member.objects.filter(pid=pid,privilege="owner")
        if member.exists():
            if member.first().uid.id==request.user.id:
                project=Project.objects.get(id=pid)
                command="docker rm -f "+project.pname
                os.system(command)

                project.delete()

                members=Member.objects.filter(pid=pid)
                members.delete()

                return Response({
                "message":"success",
            })
            else:
                return Response({
                "message":"failure",
                "data":{
                    "detail":"非项目拥有者"
                }
            })
        else:
            return Response({
                "message":"failure",
                "data":{
                    "detail":"删除时发生错误"
                }
            })  

class InviteUser(APIView):
    authentication_classes = (CsrfExemptSessionAuthentication, BasicAuthentication)
    def post(self, request, format = None):
        # 前端向后端发送项目id（pid）和被邀请的人的名称（invitename）来邀请成员进来
        user = request.user
        if not user.is_authenticated:
            return Response({
                "message":"failure",
                "data":{
                    "detail":"用户未登录"
                }
            })
        else:
            data = request.data
            pid = data.get("pid")
            invitename = data.get("invitename").strip()
            users = User.objects.filter(username=invitename)
            # invited user does not exist
            if users.count() == 0:
                return Response({
                    "message": "failure",
                    "data": {
                        "detail": "被邀请的用户不存在"
                    }
                })
            # success
            else:
                project = Project.objects.get(pk=pid)
                Member.objects.create(pid=project, uid=users.first(), privilege="collaborator")
                return Response({
                    "message":"success"
                })

class RemoveUser(APIView):
    authentication_classes = (CsrfExemptSessionAuthentication, BasicAuthentication)
    def post(self, request, format = None):
        user = request.user
        if not user.is_authenticated:
            return Response({
                "message":"failure",
                "data":{
                    "detail":"用户未登录"
                }
            })       
        else:
            data = request.data
            pid = data.get("pid")
            target_username = data.get("username")
            project = Project.objects.get(pk=pid)
            current_membership = Member.objects.filter(pid=project,uid=user).first()
            # 检查是否是这个项目的owner
            if current_membership.privilege != "owner":
                return Response({
                    "message": "failure",
                    "data":{
                        "detail": "你不是项目拥有者，不可以删除用户"
                    }
                })
            # 检查是否有这个成员
            target_user = User.objects.get(username=target_username)
            target_membership = Member.objects.filter(pid=project,uid=target_user)
            if target_membership.count() == 0:
                return Response({
                    "message": "failure",
                    "data": {
                        "detail": "该用户不在此项目中"
                    }
                })
            # owner不能删除自己
            if request.session["id"] == target_username:
                return Response({
                    "message": "failure",
                    "data": {
                        "detail": "你不能删除自己"
                    }
                })
            # 删除此成员
            target_membership.first().delete()
            return Response({
                "message":"success"
            })

class Test1(APIView):
    def get(self, request):
        return Response({
            "message": "2022.5.3 fix 403 forbidden"
        })

class Test2(APIView):
    authentication_classes = (CsrfExemptSessionAuthentication, BasicAuthentication)
    #permission_classes = [permissions.IsAuthenticated]
    def post(self, request):
        data = request.data
        param = data.get("param")
        return Response({
            "message": param
        })