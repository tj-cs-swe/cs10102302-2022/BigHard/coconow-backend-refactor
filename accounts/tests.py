import json
from re import A
from django.test import TestCase
import requests

cookie=str()

# Create your tests here.
class AccountsTest(TestCase):
    def registerTest(email,username,password):
        post_body={'email':'test@test12.com','username':'test23','password':'password'}
        post_json=json.dumps(post_body)
        r=requests.post("http://124.70.221.116:8000/api/accounts/register/",data=post_json)
        print(r.text)

    def loginTest(username,password):
        post_body={'username':'test23','password':'password'}
        post_json=json.dumps(post_body)
        r=requests.post("http://124.70.221.116:8000/api/accounts/login/",data=post_json)
        print(r.text)
        print(r.cookies)
        cookie=r.cookies

class ProjectTest(TestCase):
    def createTest(pname):
        post_body={'pname':'test'}
        post_json=json.dumps(post_body)
        r=requests.post("http://124.70.221.116:8000/api/projects/create/",data=post_json)
        print(r.text)
        print(r.cookies)

    def inviteTest(pid,uid):
        post_body={'pid':'3','uid':'3'}
        post_json=json.dumps(post_body)
        r=requests.post("http://124.70.221.116:8000/api/projects/invite/",data=post_json,cookies=cookie)
        print(r.text)
        print(r.cookies)