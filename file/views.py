from platform import java_ver
from urllib import response
from click import command
import paramiko
from django.contrib.auth.models import User
from rest_framework.response import Response
from django.http import JsonResponse,FileResponse
from rest_framework.views import APIView
from accounts.models import Member, Project
import json
import os

from rest_framework.authentication import SessionAuthentication, BasicAuthentication

from coconow.settings import UPLOAD_TEMPFILE_PATH  
class CsrfExemptSessionAuthentication(SessionAuthentication): 
    def enforce_csrf(self, request): 
        return  # To not perform the csrf check previously happening

# Create your views here.
def dockerSSH(port,command): #输入端口与命令,返回stdout
    sh = paramiko.SSHClient()  # 1 创建SSH对象
    sh.set_missing_host_key_policy(paramiko.AutoAddPolicy())  # 2 允许连接不在know_hosts文件中的主机
    sh.connect("124.70.221.116",port=port, username="root", password="root")  # 3 连接服务器
    stdin, stdout, stderr = sh.exec_command(command=command)
    return stdout.read().decode("utf-8")


class FileList(APIView):
    def get(self, request, pk):
        # 判断用户登录
        user=request.user
        if not user.is_authenticated:
            return Response({
                "message":"failure",
                "data":{
                    "detail":"用户未登录"
                }
            },status=400)
        else:
            if not Member.objects.filter(pid=pk,uid=user.id).exists():
                return Response({
                "message":"failure",
                "data":{
                    "detail":"非法访问项目",
                }
            },status=400)
            port=20000+int(pk)
            stdout=dockerSSH(port,'python /walkTree.py')
            t = json.loads(stdout)
            print("filelist",t)
            return JsonResponse({
                'filelist':json.loads(stdout),
            })


class CreateFile(APIView):
    authentication_classes = (CsrfExemptSessionAuthentication, BasicAuthentication)
    def post(self, request, pk):
        # 判断用户登录
        user=request.user
        if not user.is_authenticated:
            return Response({
                "message":"failure",
                "data":{
                    "detail":"用户未登录"
                }
            },status=400)
        else:
            if not Member.objects.filter(pid=pk,uid=user.id).exists():
                return Response({
                "message":"failure",
                "data":{
                    "detail":"非法访问项目",
                }
            },status=400)
            path=request.data.get("dir")
            name=request.data.get("name")
            dir=os.path.join(path,name)
            port=20000+int(pk)
            dockerSSH(port,'touch /home'+str(dir))
            return Response({
                "message":"success"
            })

class RemoveFile(APIView):
    def get(self, request, pk):
        # 判断用户登录
        user=request.user
        if not user.is_authenticated:
            return Response({
                "message":"failure",
                "data":{
                    "detail":"用户未登录"
                }
            },status=400)
        else:
            if not Member.objects.filter(pid=pk,uid=user.id,privilege="owner").exists():
                return Response({
                "message":"failure",
                "data":{
                    "detail":"非法删除项目或不是拥有者",
                }
            },status=400)

            path = request.GET["path"]
            port=20000+int(pk)
            dockerSSH(port,'rm /home'+str(path))
            return Response({
                "message":"success"
            })

class CreateDir(APIView):
    authentication_classes = (CsrfExemptSessionAuthentication, BasicAuthentication)
    def post(self, request, pk):
        # 判断用户登录
        user=request.user
        if not user.is_authenticated:
            return Response({
                "message":"failure",
                "data":{
                    "detail":"用户未登录"
                }
            },status=400)
        else:
            if not Member.objects.filter(pid=pk,uid=user.id).exists():
                return Response({
                "message":"failure",
                "data":{
                    "detail":"非法访问项目",
                }
            },status=400)
            path=request.data.get("dir")
            name=request.data.get("name")
            port=20000+int(pk)
            dir=os.path.join(path,name)
            print("make dir:",dir)
            dockerSSH(port,'mkdir /home'+str(dir))
            return Response({
                "message":"success"
            })

class RemoveDir(APIView):
    def get(self, request, pk):
        # 判断用户登录
        user=request.user
        if not user.is_authenticated:
            return Response({
                "message":"failure",
                "data":{
                    "detail":"用户未登录"
                }
            },status=400)
        else:
            if not Member.objects.filter(pid=pk,uid=user.id,privilege="owner").exists():
                return Response({
                "message":"failure",
                "data":{
                    "detail":"非法删除项目或不是拥有者",
                }
            },status=400)

            path = request.GET["path"]

            port=20000+int(pk)
            dockerSSH(port,'rm -rf /home'+str(path))
            return Response({
                "message":"success"
            })

class UploadFile(APIView):
    authentication_classes = (CsrfExemptSessionAuthentication, BasicAuthentication)
    def post(self, request, pk):
        # 判断用户登录
        user=request.user
        if not user.is_authenticated:
            return Response({
                "message":"failure",
                "data":{
                    "detail":"用户未登录"
                }
            },status=400)
        else:
            if not Member.objects.filter(pid=pk,uid=user.id,privilege="owner").exists():
                return Response({
                "message":"failure",
                "data":{
                    "detail":"非法访问",
                }
            },status=400)

            files = request.FILES.getlist('file')
            dir = request.POST.get('dir')
            dockername=Project.objects.get(id=pk).pname
            for f in files:
                file_path = os.path.join(UPLOAD_TEMPFILE_PATH,f.name)
                with open(file_path,'wb+') as fp :
                    for chunk in f.chunks():
                        fp.write(chunk)
                command='docker cp '+file_path+' '+dockername+':'+'/home'+dir+'/'+f.name
                os.system(command)
                os.system('rm -f '+file_path)
                

            return Response({
                "message":"success"
            })

class DownloadFile(APIView):
    def get(self, request, pk):
        # 判断用户登录
        user=request.user
        if not user.is_authenticated:
            return Response({
                "message":"failure",
                "data":{
                    "detail":"用户未登录"
                }
            },status=400)
        else:
            if not Member.objects.filter(pid=pk,uid=user.id,privilege="owner").exists():
                return Response({
                "message":"failure",
                "data":{
                    "detail":"非法删除项目或不是拥有者",
                }
            },status=400)
            
            
            path = request.GET.get('path')
            dockername=Project.objects.get(id=pk).pname
            fname=os.path.basename(path)
            fpath=os.path.join(UPLOAD_TEMPFILE_PATH,fname)
            command='docker cp '+dockername+':/home'+path+' '+fpath
            print(command)
            os.system(command)
            ret=FileResponse(open(fpath,'rb'),as_attachment=True)
            os.system('rm -f '+fpath)
            return ret

