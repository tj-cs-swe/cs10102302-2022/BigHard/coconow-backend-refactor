from asgiref.sync import async_to_sync
from channels.generic.websocket import WebsocketConsumer
from websocket_service.models import ChatRecord
from accounts.models import Project
from django.contrib.auth.models import User
from django.forms.models import model_to_dict
import json

from file.tools import *
from asgiref.sync import async_to_sync

import paramiko
import threading


class SSHRecvThread(threading.Thread):
    def __init__(self,comsumer):
        threading.Thread.__init__(self)
        self.comsumer = comsumer
    def run(self):
        while True:
            out = self.comsumer.chan.recv(1024)
            # 这里可以不用动
            if len(out) == 0:
                break
            out = out.decode("utf8")
            print('out',repr(out))
            self.comsumer.send(out)



Manager = {}

def isRetain(op):
    return type(op) == int and op>0

def isInsert(op):
    return type(op) == str

def isDelete(op):
    return type(op) == int and op<0

def applyOperation(pid,path,ops):
    manage = getFileManage(pid,path)
    index = 0
    for op in ops:
        if isRetain(op):
            index += op
        elif isDelete(op):
            manage.content = manage.content[0:index] + manage.content[index-op:]
        elif isInsert(op):
            manage.content = manage.content[0:index] + op + manage.content[index:]
    # print(manage.content)

class GroupManage:
    def __init__(self):
        self.group = {}
        self.files = {}

class FileManage:
    def __init__(self):
        self.users = {}
        self.content = ""
        self.revision = 0

def getGroupManage(pid):
    if Manager.get(pid) is None:
        Manager[pid] = GroupManage()
    return  Manager[pid]

def getFileManage(pid,path):
    GroupManage = getGroupManage(pid);
    if GroupManage.files.get(path) is None:
        GroupManage.files[path] = FileManage()
    return GroupManage.files[path]

def exitFile(pid,uid,path):
    fileManager = getFileManage(pid,path)
    fileManager.users[uid] -= 1
    if fileManager.users[uid] == 0:
        fileManager.users.pop(uid)
    if len(fileManager.users) == 0:
        filePath = tmppath(pid,path)
        with open(filePath,'w') as file:
            file.write(fileManager.content)
        dockersave(pid,path)
        removetmp(pid,path)
        Manager[pid].files.pop(path)

def openFile(pid,uid,path):
    fileManager = getFileManage(pid,path)
    print(fileManager.users)
    if len(fileManager.users) == 0:
        filepath = dockeropen(pid,path)                
        with open(filepath,'r') as file:
            fileManager.content = file.read()
    if fileManager.users.get(uid) is None:
        fileManager.users[uid] = 1
    else:
        fileManager.users[uid] += 1


def saveAllFiles(pid):
    groupManager = getGroupManage(pid)
    for path,fileManager in groupManager.files.items():
        filePath = tmppath(pid,path)
        with open(filePath,'w') as file:
            file.write(fileManager.content)
        dockersave(pid,path)



MAX_RECORD_NUM = 100


class WebsocketService(WebsocketConsumer):
    
    def connect(self):
        self.accept()
        # webssh初始化
        self.sh = paramiko.SSHClient()  # 1 创建SSH对象
        self.chan = None

    
    def disconnect(self, close_code):

        # 离开房间
        if getattr(self,"room_group_name")is not None:
            async_to_sync(self.channel_layer.group_discard)(
                self.room_group_name,
                self.channel_name
            )

        # 协作编程Manager更新
        uid = self.uid
        pid = self.pid
        if not len(self.paths) == 0:
            for path in self.paths:
                exitFile(pid,uid,path)
        groupManager = getGroupManage(pid)
        groupManager.group[uid] -= 1
        if groupManager.group[uid] == 0:
            groupManager.group.pop(uid)
        if len(groupManager.group) == 0:
            if len(groupManager.files) != 0:
                raise ValueError
            else:
                Manager.pop(pid)


    def receive(self, text_data):
        data = json.loads(text_data)
        if data['func'] == 'editors':
            delta = json.loads(text_data)
            
            type = delta['type']
            if type == 'initializeTabs':
                uid = delta['uid']
                pid = delta['pid']
                self.uid = uid
                self.pid = pid
                self.paths = []
                self.room_group_name = str(pid)

                groupManager = getGroupManage(pid)
                if groupManager.group.get(uid) is None:
                    groupManager.group[uid] = 1
                else:
                    groupManager.group[uid] += 1            

                # Join room group
                async_to_sync(self.channel_layer.group_add)(
                    self.room_group_name,
                    self.channel_name
                )

            elif type == 'initializeFile':
                uid = delta['uid']
                pid = delta['pid']
                path = delta['path']

                openFile(pid,uid,path)
                self.paths.append(path)

                fileManager = getFileManage(pid,path)
                
                self.send(text_data=json.dumps({
                    'type': 'initializeFile',
                    'content': fileManager.content,
                    'revision': fileManager.revision,
                    'path' : path
                }))

            elif type == 'saveAll':
                pid = delta['pid']
                # print(delta)
                saveAllFiles(pid)
                async_to_sync(self.channel_layer.group_send)(
                        self.room_group_name,
                        {
                            'type': 'coop_send',
                            'delta': {
                                'type': 'saveDone',
                                'pid': pid,
                            },
                            'channel_name': '',
                        }
                    )


            elif type == 'operation':
                uid = delta['uid']
                pid = delta['pid']
                path = delta['path']
                fileManager = getFileManage(pid,path)



                if delta['revision'] == fileManager.revision:
                    fileManager.revision += 1
                    applyOperation(pid,path,delta['operation'])
                    # time.sleep(1)
                    print("send ack to",self.channel_name,"in",self.room_group_name)
                    self.send(text_data=json.dumps({'type' : 'ack', 'path' : path}))

                    async_to_sync(self.channel_layer.group_send)(
                        self.room_group_name,
                        {
                            'type': 'coop_send',
                            'delta': delta,
                            'channel_name':self.channel_name,
                        }
                    )
                else:
                    print("nak delta",delta)
                    # time.sleep(1)
                    self.send(text_data=json.dumps(
                        {'type' : 'nak', 'path' : path }
                    ))
                # saveAllFiles(pid)


            print("receive delta",self.room_group_name,self.channel_name,delta)


        elif data['func'] == 'chat':
            delta = json.loads(text_data)
            # t1 = time.time()
            
            type = delta['type']
            uid = delta['uid']
            pid = delta['pid']
            if type == 'initialize':
                self.uid = uid
                self.pid = pid
                self.room_group_name = str(pid)

                # Join room group
                async_to_sync(self.channel_layer.group_add)(
                    self.room_group_name,
                    self.channel_name
                )

                record_set = ChatRecord.objects.filter(pid=pid).order_by('timestamp')
                record_data = []
                record_num = len(record_set)
                l = max(record_num-MAX_RECORD_NUM,0)
                for index in range(l,record_num):
                    record = record_set[index]
                    data = model_to_dict(record,fields=['pid','uid','message'])
                    data['timestamp'] = record.timestamp.strftime('%Y-%m-%d %H:%M:%S')
                    # print(data)
                    record_data.append(data)
                    


                self.send(text_data=json.dumps({
                    'type': 'initialize',
                    'records': record_data
                }))

            elif type == 'message':
                message = delta['message']
                project = Project.objects.get(id=pid)
                user = User.objects.get(id=uid)
                ChatRecord.objects.create(pid=project,uid=user,message=message)

                async_to_sync(self.channel_layer.group_send)(
                self.room_group_name,
                {
                'type': 'coop_send',
                'delta': delta,
                'channel_name':self.channel_name,
                }
                )
            
            print("receive message",self.room_group_name,self.channel_name,delta)
        
        
        elif data['func'] == 'docker':
            if data['type'] == 'pid':
                self.sh.set_missing_host_key_policy(paramiko.AutoAddPolicy())  # 2 允许连接不在know_hosts文件中的主机
                self.sh.connect("124.70.221.116",port=20000+int(data['pid']),username="root", password="root")  # 3 连接服务器
                self.chan=self.sh.invoke_shell(term='xterm')
                self.recvThread = SSHRecvThread(self)
                self.recvThread.setDaemon(True)
                self.recvThread.start()
                self.chan.send('cd /home\n')
                print("连接成功")
            elif data['type'] == 'opt':
                if self.chan is None:
                    print("error!")
                    self.send('no connect!')
                opt = data['opt']
                self.chan.send(opt)


