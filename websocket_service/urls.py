from django.urls import path
from websocket_service.WebSSH import WebSSHService
from websocket_service.CoProgramming import CoProgrammingConsumer
from websocket_service.wbhtest import test
from websocket_service.ChatRoom import ChatRoomConsumer
from websocket_service.all_websocket import WebsocketService
websocket_url = [
    # path("ws/",CoProgrammingConsumer.as_asgi()),
    # path("web/",WebSSHService.as_asgi()),
    # path("chat/",ChatRoomConsumer.as_asgi()),
    # path("wbh/",test.as_asgi()),
    path("all/",WebsocketService.as_asgi()),
]